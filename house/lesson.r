#����� ����� ������� ������ 
loan.raw <- read.csv('loan.csv')

#���� �� ����� �� ������ �������
str(loan.raw)

#�����  �� ��� ����� �� �� ����� �� ��� �����
summary(loan.raw)

#������� ������ �� X ��� ��� ������ ��� ��� �� ��� �� ������ �� X 
make_unknown <- function(x){
  if(x=="") return ('unknown')
  return(x)
}

loan.prepared <- loan.raw

loan.prepared$Self_Employed <- as.character(loan.prepared$Self_Employed)
loan.prepared$Self_Employed <- sapply(loan.prepared$Self_Employed,make_unknown)
#����� ������ ������

loan.prepared$Self_Employed <- as.factor(loan.prepared$Self_Employed)

loan.prepared$Dependents <- as.character(loan.prepared$Dependents)
loan.prepared$Dependents <- sapply(loan.prepared$Dependents,make_unknown)
#����� ������ ������
loan.prepared$Dependents <- as.factor(loan.prepared$Dependents)


loan.prepared$Married <- as.character(loan.prepared$Married)
loan.prepared$Married <- sapply(loan.prepared$Married,make_unknown)
#����� ������ ������
loan.prepared$Married <- as.factor(loan.prepared$Married)

loan.prepared$Gender <- as.character(loan.prepared$Gender)
loan.prepared$Gender <- sapply(loan.prepared$Gender,make_unknown)
#����� ������ ������
loan.prepared$Gender <- as.factor(loan.prepared$Gender)

str(loan.prepared)
summary(loan.prepared)

any(is.na(loan.prepared$ApplicantIncome))
any(is.na(loan.prepared$CoapplicantIncon))
any(is.na(loan.prepared$LoanAmount))

make_average <- function(x,xvec){
  if(is.na(x)) return(mean(xvec,na.rm = T))
  return(x)
}

loan.prepared$LoanAmount <- sapply(loan.prepared$LoanAmount,make_average,xvec =loan.prepared$LoanAmount )
loan.prepared$Loan_Amount_Term <- sapply(loan.prepared$Loan_Amount_Term,make_average,xvec =loan.prepared$Loan_Amount_Term )
loan.prepared$Credit_History <- sapply(loan.prepared$Credit_History,make_average,xvec =loan.prepared$Credit_History )

#EDA - Exploratory Data Analysis

library(ggplot2)
#how education efects
ggplot(loan.prepared, aes(Education)) + geom_bar(aes(fill = Loan_Status))
ggplot(loan.prepared, aes(Education)) + geom_bar(aes(fill = Loan_Status), position = 'fill')

#how gender affects loan status
ggplot(loan.prepared, aes(Gender)) + geom_bar(aes(fill = Loan_Status))
ggplot(loan.prepared, aes(Gender)) + geom_bar(aes(fill = Loan_Status), position = 'fill')

#how dependents affects loan status
ggplot(loan.prepared, aes(Dependents)) + geom_bar(aes(fill = Loan_Status))
ggplot(loan.prepared, aes(Dependents)) + geom_bar(aes(fill = Loan_Status), position = 'fill')

#how Credit_History affects loan status
ggplot(loan.prepared, aes(as.factor(Credit_History))) + geom_bar(aes(fill = Loan_Status))
ggplot(loan.prepared, aes(as.factor(Credit_History))) + geom_bar(aes(fill = Loan_Status), position = 'fill')

#turn credit history into a factor 

loan.prepared$Credit_History <- as.factor(loan.prepared$Credit_History)

summary(loan.prepared)

ggplot(loan.prepared, aes(ApplicantIncome, CoapplicantIncome)) + geom_bin2d(aes(fill = Loan_Status), binwidth = c(1000,1000)) + xlim(0,20000) + ylim(0,10000)

#build model 
loan.prepared$Loan_ID <- NULL
install.packages('caTools')
library('caTools')

filter <- sample.split(loan.prepared$Married, SplitRatio = 0.7)

loan.train <- subset(loan.prepared, filter = T)
loan.test <- subset(loan.prepared, filter = F)

dim(loan.train)
dim(loan.test)

loan.model <- glm(Loan_Status~., family = binomial(link = 'logit'), data = loan.train)

summary(loan.model)

