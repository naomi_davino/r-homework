
titanic_df <- read.csv('train.csv')

#Q1
na_col <- function(df){
  apply(df,2,function(df)any(is.na.data.frame(df)))
}

na_vec<-function(df){
  return(as.vector(na_col(df)))
}

#Q2
check_na <- na_vec(titanic_df)
i <-which(check_na==TRUE)

med_na <- function(vec){
  ind <- which(is.na(vec))
  vec[ind] <- median(vec,na.rm = TRUE)
  return(vec)
}
titanic_df[,i]<-med_na(titanic_df[,i])

#Q3
str(titanic_df)
titanic_df[,2] <- factor(titanic_df[,2], levels=c(0,1),  labels = c( "no", "yes"))
titanic_df[,3] <- factor(titanic_df[,3], levels=c(1,2,3),  labels = c( "class A", "class B", "class C"))

