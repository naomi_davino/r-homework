
house <- read.csv('kc_house_data.csv')

str(house)
summary(house)

#load ggplot2
# install.packages('ggplot2') #����� ������
library(ggplot2) # ����� ������

#install.packages('caTools') #����� �������
library(caTools)

#creating year,month and day columns
year <- substr(house$date,1,4)
month <- substr(house$date,5,6)
day <- substr(house$date,7,8)
house$year <- year
house$month <- as.factor(month)
house$day <- day

#Remove irrelevent columns
house$zipcode <- NULL
house$lat <- NULL
house$long <- NULL
house$date <- NULL


#how month affects price
ggplot(house, aes(month,price)) + geom_boxplot() 

#how bedrooms affects price
ggplot(house, aes(house$bedrooms,house$price)) + geom_point() + stat_smooth(method = lm)

#how view affects price
ggplot(house, aes(house$view, house$price)) + geom_point() + stat_smooth(method = lm)

#how sqft living15 affects price
ggplot(house, aes(house$sqft_living15,house$price)) + geom_point() + stat_smooth(method = lm)

#how grade affects price
ggplot(house, aes(house$grade, house$price)) + geom_point() + stat_smooth(method = lm)

#how yr_built affects price
ggplot(house, aes(house$yr_built,house$price)) + geom_point() + stat_smooth(method = lm)

house.prepared <- house

#���� �� 70 ���� ������ ����� �� 30 ���� ������
#����� ������� ��� ������� 0.7,0.3
#�� ���� ���� ����� ���� ��������, ���� ����� ���� �� ���� �������� ���� �� ���� / ���� �������

filter <- sample.split(house.prepared$grade, SplitRatio = 0.7)

house.train <- subset(house.prepared, filter == TRUE)
house.test <- subset(house.prepared, filter == FALSE)

dim(house.prepared) #���� ��� ������ ������ �� ��� ������
dim(house.train)
dim(house.test)

model <- lm(price~. ,house.prepared)
summary(model)

predicted.train <- predict(model, house.train) #����� ����� �������
predicted.test <- predict(model, house.test) #����� ����� ����

MSE.train <- mean((house.train$price-predicted.train)**2) #����� ��� ������
MSE.test <- mean((house.train$price-predicted.test)**2)


MSE.train**0.5 #���� ������
MSE.test**0.5

mean(house.prepared$price)


# (season = grade       count = view)


